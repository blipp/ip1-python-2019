table = []

ord_0 = ord("0")
ord_9 = ord("9")

def table_from_string(s):
    global table
    l = [[]]
    j = 0
    for i in range(0, len(s)):
        if ord_0 <= ord(s[i]) <= ord_9:
            # c'est un chiffre
            l[j] += [int(s[i])]
        elif s[i] == ",":
            j += 1
            l += [[]]
    table = l

s = "123,456,39,8,,9"
table_from_string(s)
print(table)

def string_from_table():
    s = ""
    for i in range(0, len(table)):
        for j in range(0, len(table[i])):
            s += str(table[i][j])
        if not i == len(table) - 1:
            s += ","
    return s

print(string_from_table())

def table_is_square():
    l = len(table)
    for i in range(0, len(table)):
        if not len(table[i]) == l:
            return False
    return True

print(table_is_square())

table_from_string("1234,4567,7890,4568")
print(table_is_square())

# 5 minutes

def square_table_is_sym():
    l = len(table)
    for i in range(0, l-1):
        for j in range(i+1, l):
            if not table[i][j] == table[j][i]:
                return False
    return True

table_from_string("1234,2978,3756,4861")
print(square_table_is_sym())
table_from_string("1234,2978,3756,4851")
print(square_table_is_sym())
table_from_string("123,256,367")
print(square_table_is_sym())
table_from_string("123,256,397")
print(square_table_is_sym())

def zip(listes):
    l = 0
    for i in range(0, len(listes)):
        l += len(listes[i])
    z = [0]*l
    pointer = 0
    index = 0
    while pointer < l:
        for i in range(0, len(listes)):
            if index < len(listes[i]):
                z[pointer] = listes[i][index]
                pointer += 1
        index += 1
    return z

print(zip([[], [21], [1, 2, 3, 4], [5, 6, 7, 8, 9], [17, 12]]))
