# exo 1
def reponse(n):
    return 42//n


# exo 2
def somme_fizzbuzz(n):
    somme = 0
    for i in range(1, n):
        if i % 3 == 0 or i % 5 == 0:
            somme = somme + i
    return somme


# exo 3
def arrow(n, left):
    if left:
        print("<", end="")
    for i in range(0, n):
        print("=", end="")
    if not left:
        print(">", end="")
    print()


# exo 4
ord_a = ord("a") # 97
ord_z = ord("z") # 122
ord_A = ord("A") # 65
ord_Z = ord("Z") # 90

def compteur(s):
    l = len(s)
    if l == 0:
        print("chaîne vide")

    minus = 0
    majus = 0
    special = 0

    for i in range(0, l):
        if ord_a <= ord(s[i]) <= ord_z:
            minus = minus + 1
        elif ord_A <= ord(s[i]) <= ord_Z:
            majus = majus + 1
        else:
            special = special + 1

    if minus > 0:
        print(minus, "caractères minuscules")
    if majus > 0:
        print(majus, "caractères majuscules")
    if special > 0:
        print(special, "caractères spéciaux")


#exo 5
def fibo(n):
    moins2 = 1
    moins1 = 1
    new = 0
    somme = 2
    for i in range(3, n+1):
        new = moins1 + moins2
        moins2 = moins1
        moins1 = new
        if new % 2 != 0:
            somme = somme + new
    return somme


# Tests et affichages

# exo 1
print(reponse(21))
print(reponse(15))
print(reponse(7))

# exo 2
print(somme_fizzbuzz(1000))

# exo 3
arrow(10, False)
arrow(1, True)

# exo 4
compteur("Bonjour le monde !")
print()
compteur("écoute")
print()
compteur("WHAT?")
print()
compteur("$$")
print()
compteur("")

# exo 5
print(fibo(6))
print(fibo(100))
